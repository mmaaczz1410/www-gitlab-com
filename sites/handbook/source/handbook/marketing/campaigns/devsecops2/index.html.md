---
layout: handbook-page-toc
title: "DevSecOps 2.0 Campaign"
description: "Everything you need to know about the DevSecOps 2.0 campaign."
twitter_image: '/images/tweets/handbook-marketing.png'
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .toc-list-icons .hidden-md .hidden-lg}

## Campaign Overview
{: #overview .alert .alert-gitlab-orange}

### Business Objectives
{: #business-objectives}
<!-- DO NOT CHANGE THIS ANCHOR -->
1. Get Security DMs and Manager of App Dev to convert on our Security assets
2. Help sales land Ultimate deals by highlighting the value of a single DevSecOps platform

### Campaign Visual
{: #campaign-visual}
<!-- DO NOT CHANGE THIS ANCHOR -->

### Campaign Pages & Activities
{: #campaign-activities}
<!-- DO NOT CHANGE THIS ANCHOR -->

**[See the FY22 Marketing Calendar for upcoming virtual events >>]()**

Always On Lead Generation:
* :books: Content | 
* :books: Content | 
* :video_camera: On-Demand Webinar | 

## Research & Prep
{: .alert #research-prep .alert-gitlab-orange}

### Persona & Positioning
{: #persona-positioning .gitlab-purple}
<!-- DO NOT CHANGE THIS ANCHOR -->

Users include both the developer and the security pro. We pride ourselves on having a united view of the software vulnerabilies and their status toward resolution. The buyer persona is usually the security manager who funds the delta from Premium to Ultimate.

#### Target Persona 1:

* **Level: Manager or Director**
* **Function: Application Security**. In large organizations, Application Security is a dedicated team or person. In smaller IT shops a group or individual security person may be responsible for application security along with network security, security operations, and more.
While developers and DevOps teams like to use GitLab for security, the security pro is often skeptical, comparing it to their favorite incumbant scanner. They may have bet their career on justifying a very expensive scanner like Fortify or Veracode and are often reluctant to replace it, even if it can simplify their work as well as that of the developer.

* **Challenges we're trying to solve with this campaign:**  
   Security struggles to fit traditional application scanning methods into an iterative, agile development environment. 
     1) Vulnerabilities often discovered in production causing project delays because security finds issues just before go-live
     2) Friction in the workflow causes rework and wastes time understanding context of a vuln reported today that was created a week ago.
     3) Business improvements (a VP App goal) take a back seat to fixing security vulns (not my goal - this is a goal of the CISO) creating adversaries between groups.
     4) Dependency backlog continues to grow (technical debt)
     5) Integrating incumbent scanners into CI pipelines is complex with often unpredictable app sec license costs.  


   Recent software supply chain attacks are raising awareness of the need for greater application security that includes not only app sec testing but also policy automation, common compliance controls, and improved visibility and control over the SDLC tools and cloud native infrastructure. The USA Executive Order on Cybersecurty will only heighten the need for better security and compliance processes. Complexity is becoming an even bigger challenge!
  
* **How GitLab helps:** Forrester research shows that complexity is one of the biggest challenges facing CISO's today. GitLab provides:
   * **simplicity** - less tool integration and maintenance, one predictable cost
   * earlier **visibility** into risk by scanning earlier and using single source of truth between dev and sec
   * greater **control** over the security of the SDLC through use of one platform with end-to-end policies and auditability.  


* **Why is GitLab a better solution than competitors:** 
1. For those struggling to 'shift left', GitLab can provide a path of least resistence: Developers already love GitLab and Security teams can harness that momentum to quickly scale their app sec program while adding defense-in-depth of multiple scan types (SAST, DAST, Dependencies, Containers, and License Compliance, Secrets detection, and fuzz testing) built-in and automatically applied. Security is more efficient with one UI to see all findings in one place, along with details about the vulnerabilities and their remediation efforts.  
2. For those already trying to embed security within their CI pipeline, GitLab simplifies the complexity by offering one solution for all of your app sec testing needs, allowing you to test every code change with straightforward and predictable seat-based pricing.
3. A single platform can greatly simplify compliance and audit requirements allowing you to apply policies automatically for greater consistency and see who changed what/where/when across the SDLC. 
4. GitLab not only helps your enterprise delivery more secure code, but also manage and monitor containers and kubernetes upon which cloud native apps depend.

Competitive solutions require substantial set-up and maintenance to integrate scans into the CI pipeline, with no end-to-end visibility across mutliple tools.

#### Target Persona 2:

* **Level: Practitioner**
* **Function: Application Security**
The security pro cares most about managing risk to the enterprise/agency. They take a broad view of process looking for process improvement areas to reduce risk and avoid repeat mistakes. Because they care about risk, they want to identify unresolved vulnerabilities, their severity, and their remediation status. They care about trends over time and aggregate improvements. Often their metrics are mean time to remediation. It is rare that the security person themselves is able to remediate a software security flaw; they depend upon the developer to do this. This goal misalignment is often a reason for contention between the groups. In traditional app sec environments, where testing is done at the end of the SDLC, they may spend alot of their time tracking and reporting vulnerability statuses, vetting findings, and triaging to dev teams. Where development is more automated, they may be able to focus more on setting policies and allowing the tools to enforce them. They often want to avoid moving any new critical/high vulnerabilities into production and favor breaking the build to enforce this.

* **Challenges we're trying to solve with this campaign:** 

Security struggles to fit traditional application scanning methods into an iterative, agile development environment. 
  1) Vulnerabilities ar4e often discovered in production, or right before, causing project delays because security finds issues just before go-live. The security practitioner becomes 'the bad guy', stopping a launch.
  2) Siloed tools waste time understanding context of a vuln reported today that was created a week ago and translation may be required between findings from a security tool and where the flaw resides in the code.
  3) The growing use of dependencies, APIs, containers, Kubernetes adds attack surfaces to assess and monitor while security teams struggle to keep up. Sadly, some practitioners may not even know they have this problem.
  4) Tracking remediation status is difficult - often many spreadsheets and constantly asking others for updates.
  5) Keeping up with wildly different licensing between multliple security scanners can waste time determining if current licensing is sufficient for each new project. 
  6) Security pros are outnumbered by the development teams they are aligned to - frustration, overworked.

Recent software supply chain attacks are raising awareness of the need for greater application security that includes not only app sec testing but also policy automation, common compliance controls, and improved visibility and control over the SDLC tools and cloud native infrastructure. More pressure on an already difficult job!
  
* **How GitLab helps:** 
   * earlier **visibility** into risk by scanning earlier and using single source of truth between dev and sec
   * greater **control** over the security of the SDLC through use of one platform with end-to-end policies and auditability.  

* **Why is GitLab a better solution than competitors:**
1. For those struggling to 'shift left', GitLab can provide a path of least resistence: Developers already love GitLab and Security teams can harness that momentum to quickly scale their app sec program while adding defense-in-depth of multiple scan types (SAST, DAST, Dependencies, Containers, and License Compliance, Secrets detection, and fuzz testing) built-in and automatically applied. 
2. GitLab can help Security be more efficient with one UI to see all findings in one place, along with details about the vulnerabilities and their current remediation efforts. The A-F grading scale on the Security Dashboard helps security quickly identify projects with the most risk.
3. GitLab's single platform can greatly simplify compliance controls allowing you to enforce scan requirements and other controls consistently across projects. A united tool offers insight into dev actions and remediation status. Security can be a better helper not a hindrance/inspector.
4. GitLab not only helps your enterprise delivery more secure code, but also manage and monitor containers and kubernetes upon which cloud native apps depend.

Competitive solutions require substantial set-up and maintenance to integrate scans into the CI pipeline, with no end-to-end visibility across mutliple tools.


#### Target Persona 3:

* **Level: Manager**
* **Function: Application Development or DevOps**   
The developer cares about security but does not want to become a security expert. Their primary driver to write secure code is to protect their personal/professional reputation. They don't want to be the one that brings their company down via vulnerable code that they wrote. At the same time, they are goaled mostly on quickly turning out code that meets their users' requirements. Often they are not measured on security flaws. Security can seem like a necessary nuisance. Tools that fit within their workflow, without context-switching are most acceptable. The clarity GitLab provides by reporting vulnerabilities at code commit (changes they just made, not someone else's) is helpful.


* **Challenges we're trying to solve with this campaign:** 

Security is important but we need new tools/methods in order to fit it into an iterative, agile development environment. Traditional app sec frustrates development efforts:
  1) Vulnerabilities often discovered in production causing project delays because security finds issues just before go-live
  2) Friction in the workflow causes rework and wastes time understanding context of a vuln reported today that was created a week ago.
  3) Business improvements (a VP App goal) take a back seat to fixing security vulns (not my goal - this is a goal of the CISO) creating adversaries between groups.
  4) Security finds everything - but too late - it becomes technical debt to go back and fix later (and potential liability too).
  5) Some security teams actually want developers to use the security scanners (like Fortify) directly!

Recent software supply chain attacks are raising awareness of the need for greater application security that includes not only app sec testing but also protecting the software supply chain. The USA Executive Order on Cybersecurty will only heighten the need for better security and compliance processes. I have a feeling things will get more complex with more people looking over my shoulder and more controls hindering my development efforts.
  
* **How GitLab helps:** 
   * **simplicity** - I can stick with a tool I already use + less tool integration and maintenance, let's us focus on business needs not tool chains   
   * earlier **visibility** into risk with all scan results within the MR pipeline where I'm already working. No context switching. And because security teams can see what vulns remain when I'm done, along with remediation efforts, they don't constantly pull me away from my work asking for updates.
   * **control** is built in. I don't have to hunt for policies. Requirements can be automatically applied via compliant pipelines and exceptions easily tracked.  


* **Why is GitLab a better solution than competitors:** 

   1.  GitLab automatically provides security scans, seamlessly helping me find and fix my own security flaws. There is no added work to set these up, no new tools to learn, no context-switching. Comparitively, other build-your-own solutions require significant set-up effort. GitLab's AutoDevOps simplifies the set up of all of these scans into one command.
   2. By providing the scan results within the merge request pipeline, the developer does not need to change context or use another tool. The results show vulnerabilities they created - not ones lurking for years in the code or that another developer created. It's easier to fix what I'm working on now then when someone asks me weeks later. 
   3. Built-in fuzz testing makes it easier to apply a pretty advanced method without much learning ramp. It can help me find quality/logic flaws as well.
   4. Transparency of who changed what/where/when can save me time later when audit comes around asking questions.
    


### Keyword Research
{: #keyword-research .gitlab-purple}
<!-- DO NOT CHANGE THIS ANCHOR -->

**[See the DevSecOps 2.0 keyword research doc >>](https://docs.google.com/spreadsheets/d/1yaozMlR3SpxfX1_rMVWEhJTfMgT5sXwpkfXO9WOwSck/edit#gid=0)**

### Polished Messaging
{: #polished-messaging .gitlab-purple}
<!-- DO NOT CHANGE THIS ANCHOR -->
#### Overall

|  | Polished Messaging |
| ------ | ------ |
| **Overall Message** | Bolster security and streamline the SDLC by embedding security into DevOps |
| **Headline** | Harness the developer's existing workflow to scale your application security program |
| **Statement** | GitLab automates robust application security scanning, embedded within CI, to help you align your security program to an iterative agile development process. |
| **Key Messages** | 1. As developers push code faster and in smaller increments, you need application scanning that can keep up. GitLab scans run quickly by scanning the code that changed.|
||2. GitLab provides SAST, DAST, Container and Dependency Security scanning within the DevOps CI workflow, out of the box, without building pipelines yourself. |
|| 3. Empower developers to find and fix vulnerabilities before their code is merged with others. |
|| 4. Scan every code change before it leaves the developer's hands, simplifying and even automating remediation. |
|| 5. Embedded vs integrated security. What GitLab does, we embed security. VS What others do (and probably what is most searched for) is integrated security (must be set up).|
| **Topics** | 1. DevSecOps / security embedded within CI pipelines|
||2. Managing the growing risk and scaling remediation of third-party code vulnerablities.  |
||3. Container security |
||4. API Security|
| **Top-level Keywords**  | DevSecOps, application security, application security testing, software security, dependency scanning, container scanning, container security |

| Theme | Digestable Sound bytes |
| ------ | ------ |
| Embed security within CI pipelines | Embedded security will streamline your software development lifecycle, allowing for faster feedback and faster cycle times. |
| | DevSecOps with GitLab will help customers shift left, enabling them to find and resolve security vulnerabilities earlier in the DevOps lifecycle. |
||Stop managing complex tool chain plug-ins and fragile automation scripts when you embed security. Don't just integrate it.|
|A single source of truth for DevSecOps|GitLab is a single tool for the entire SDLC – including security. Increase transparency and collaboration between teams by providing team members with a single view of each project.|
||Harness the developer's existing workflow to scale your application security program, improve efficiency, and keep security team members in the loop.|
|Scan code at every commit|Automate security scans at every commit to ensure that all code can be reviewed at scale and that developers receive immediate feedback. |
||Reduce delays in production by finding and fixing vulnerabilities in development.|
|Reduce risk with DevSecOps|Scan more code faster with GitLab Secure embedded into your DevOps workflow.|

## Buyer Journeys, Content & Emails
{: .alert #content-emails .alert-gitlab-orange}

### Prescriptive Buyer Journeys
{: #buyer-journey .gitlab-purple}
<!-- DO NOT CHANGE THIS ANCHOR -->

#### Developers
{: #buyer-journey-ic}
<!-- DO NOT CHANGE THIS ANCHOR -->
| Stage | Asset 1 | Asset 2| Asset 3 |
| ------ | ------ | -------| -------|
| Awareness|  |  | |
| Consideration |  |  | |
| Purchase| | | |

#### Individual Contributor (Security)
{: #buyer-journey-ic}
<!-- DO NOT CHANGE THIS ANCHOR -->
| Stage | Asset 1 | Asset 2| Asset 3 |
| ------ | ------ | -------| -------|
| Awareness|  |  | |
| Consideration |  |  | |
| Purchase| | | |


#### Manager (Security)
{: #buyer-journey-ic}
<!-- DO NOT CHANGE THIS ANCHOR -->
| Stage | Asset 1 | Asset 2| Asset 3 |
| ------ | ------ | -------| -------|
| Awareness|  |  | |
| Consideration |  |  | |
| Purchase| | | |

#### Manager (Development or DevOps)
{: #buyer-journey-ic}
<!-- DO NOT CHANGE THIS ANCHOR -->
| Stage | Asset 1 | Asset 2| Asset 3 |
| ------ | ------ | -------| -------|
| Awareness|  |  | |
| Consideration |  |  | |
| Purchase| | | |

### Marketo Nurture Emails
{: #nurture-emails .gitlab-purple}
<!-- DO NOT CHANGE THIS ANCHOR -->

**[See Marketo Nurture Email Copies >>](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/-/issues/2478#ship-final-copies)**


### Outreach Emails
{: #outreach-emails .gitlab-purple}
<!-- DO NOT CHANGE THIS ANCHOR -->


## Behind the Scenes
{: .alert #research-prep .alert-gitlab-orange}

#### Campaign Execution Documentation & Resources
{: #campaign-execution .gitlab-purple}
<!-- DO NOT CHANGE THIS ANCHOR -->
* [Campaign Epic](https://gitlab.com/groups/gitlab-com/marketing/-/epics/2201)
* [Project timeline plan](https://docs.google.com/spreadsheets/d/17jpqNUlldGaQ7EMEUEPGGWH-7JqIpuTDDBaOsNECNpE/edit#gid=1019466600)
* [Copy Doc]()
* [Art Work]()
